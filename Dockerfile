###############################################################################
# WikiGenBase
# The base image, used for most of the testing and building.
###############################################################################
FROM archlinux/base:latest
LABEL maintainer="sakul6499.lukas@gmail.com" 

# Debug infos + update and install basic packages
USER root
RUN uname -a \
 && pacman --version \
 && pacman -Syu --noconfirm base-devel sudo mlocate git vim \
 && groupadd sudo

# Create 'yay' user & add sudoers entry
RUN useradd --create-home --groups sudo,root --shell=/bin/false yay
RUN echo "yay ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN echo "sudo ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
RUN echo "root ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# install 'yay'
USER yay
RUN cd /home/yay \
 && git clone https://aur.archlinux.org/yay.git \
 && cd yay \
 && makepkg -si --noconfirm \
 && yay --version

# install cmake and kotlin-native-git
RUN yay -S --noconfirm cmake kotlin-native-bin

# Update mlocate-database
USER root
RUN updatedb -v

